<?php
return [
   'host' => env('REGISTRATION_API_HOST', 'example.com/'),
   'url_lifetime' => env('REGISTRATION_API_URL_LIFETIME', 600)
];
