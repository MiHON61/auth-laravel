<?php

namespace App\Providers;

use App\Services\RegistrationService;
use App\Services\Contracts\RegistrationServiceInterface;
use Illuminate\Support\ServiceProvider;

class RegistrationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            RegistrationServiceInterface::class,
            function ($app) {
                return new RegistrationService(config('registration_service'));
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
