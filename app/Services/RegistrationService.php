<?php
namespace App\Services;

use App\Models\User;
use App\Services\Contracts\RegistrationServiceInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class RegistrationService implements RegistrationServiceInterface
{
    /**
     * The registration api configuration
     *
     * @var array
     */
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Generating user's login url
     * @param   App\Models\User $user
     * @return string
     */
    public function generateUrl(User $user):string
    {
        $userTokens = $user->tokens;

        foreach ($userTokens as $token) {
            $token->revoke();
        }

        $userKey = Str::uuid();
        Cache::put($userKey, $user, $seconds = $this->config['url_lifetime']);
        $url = $this->config['host'].'authenticate/'.$userKey;

        return $url;
    }

    /**
     * Search for a user from the cache by key
     * @param  string $key
     * @return  App\Models\User
     */
    public function getUserByKey($key):User
    {
        $user=  Cache::pull($key);
        
        if (!$user) {
            throw new UnprocessableEntityHttpException('Wrong key');
        }

        return $user;
    }
}
