<?php
namespace App\Services\Contracts;

use App\Models\User;

interface RegistrationServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | Generate url
    |--------------------------------------------------------------------------
    |
    | Here you can generate user's login url for its subsequent submission
    |
    */
    public function generateUrl(User $user);
    /*
    |--------------------------------------------------------------------------
    | Get user by key
    |--------------------------------------------------------------------------
    |
    | Here you can find user from cache by key
    |
    */
    public function getUserByKey($key);
}
