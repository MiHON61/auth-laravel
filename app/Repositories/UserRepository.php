<?php
namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Log;

class UserRepository implements UserRepositoryInterface
{
    public function getByEmail($email):User
    {
        return User::where('email', $email)->firstOrFail();
    }

    public function editUser($userId, $data)
    {
        $user= User::findOrFail($userId);
        $user->fill($data);
        $user->save();
        return $user;
    }
}
