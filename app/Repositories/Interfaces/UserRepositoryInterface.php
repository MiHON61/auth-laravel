<?php
namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function getByEmail($email);

    public function editUser($userId, $data);
}
