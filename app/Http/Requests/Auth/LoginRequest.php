<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
        ];
    }

    /**
     * Change default validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.exists' => 'Email not found',
        ];
    }
}
