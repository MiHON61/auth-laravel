<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
        ];
    }

    /**
     * Change default validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique' => 'This email is already exists',
        ];
    }
}
