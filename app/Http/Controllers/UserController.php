<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UpdateRequest;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * The user repository implementation.
     *
     * @var App\Repositories\Interfaces\UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Validating data and updating user
     * @param  App\Http\Requests\User\UpdateRequest $request
     * @return array
     */
    public function update(UpdateRequest $request):array
    {
        $user = Auth::user();
        $data = Arr::only($request->toArray(), ['email']);
        $updatedUser = $this->userRepository->editUser($user->id, $data);
        $updatedUser = Arr::only($updatedUser->toArray(), ['email', 'name']);

        return $updatedUser;
    }
}
