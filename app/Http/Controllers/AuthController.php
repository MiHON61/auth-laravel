<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Notifications\LoginNotification;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Contracts\RegistrationServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AuthController extends Controller
{
    /**
     * The user repository implementation.
     *
     * @var App\Repositories\Interfaces\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * The registration service implementation.
     *
     * @var App\Services\Contracts\RegistrationServiceInterface
     */
    private $registrationService;

    public function __construct(UserRepositoryInterface $userRepository, RegistrationServiceInterface $registrationService)
    {
        $this->userRepository = $userRepository;
        $this->registrationService = $registrationService;
    }

    /**
     * Validating email and send login url to user
     *
     * @param  App\Http\Requests\Auth\LoginRequest $request
     * @return void
     */
    public function login(LoginRequest $request):void
    {
        $user = $this->userRepository->getByEmail($request->email);
        $url = $this->registrationService->generateUrl($user);
        $user->notify(new LoginNotification($url));
    }

    /**
     * Validating key, generating token and returning it with user's data
     *
     * @param  Illuminate\Http\Request $request
     * @return array
     */
    public function keyAuthentication(Request $request):array
    {
        $user = $this->registrationService->getUserByKey($request->key);
        $token = $user->createToken('Auth token')->accessToken;
        $user= Arr::only($user->toArray(), ['name', 'email']);

        return ['user'=>$user, 'token'=>$token];
    }
}
