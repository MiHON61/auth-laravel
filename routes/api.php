<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Validating the user's email, if successful, sending him a login link and return 200OK
Route::post('/login', [AuthController::class, 'login']);

//User key verification, if successful, returning token and user data
Route::post('/key_authentication', [AuthController::class, 'keyAuthentication']);

//Validating is the mail is unique, if successful, editing a user's mail and returning new user's data
Route::put('/user/edit', [UserController::class, 'update'])->middleware('auth:api');
